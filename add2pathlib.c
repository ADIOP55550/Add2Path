#include "add2pathlib.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/file.h>
#include <errno.h>
#include <ctype.h>
#include <limits.h>
#include <unistd.h>

#include "a2p_errors.h"
#include "c-vector/vec.h"

#define insert_begin "export PATH=\"$PATH:"
#define insert_end "\"\n"

//define TOK_DEBUG

int resolvePath(PTH path, char *buffer){
  realpath(path, buffer);
  /* printf("Resolved %s to %s\n", path, buffer); */
  if (errno != 0)
    return ERR_EXTERNAL;
  return 0;
}

bool isPathRelative(PTH path, int *error) {

  if (path == NULL || *path == '\0') {
    // strlen(path) == 0
    a2p_errno = 1;
    *error = ERR_EMPTY_PTH;
    return false;
  }

  // Path is absolute if it starts with '/'
  return *path != '/';
}


bool isPathAbsolute(PTH path, int *error){

  if (path == NULL || *path == '\0') {
    // strlen(path) == 0
    a2p_errno = 1;
    *error = ERR_EMPTY_PTH;
    return false;
  }

  return *path == '/';
}

PATH_TYPE getPathType(PTH path, int *error) {
  struct stat s;

  if (path == NULL || *path == '\0') {
    *error = ERR_EMPTY_PTH;
    return PT_ERROR;
  }

  if (stat(path, &s) == 0) {
    if (s.st_mode & S_IFDIR)
      return PT_DIRECTORY;
    else if (s.st_mode & S_IFREG)
      return PT_FILE;
    else
      return PT_OTHER;
  }

  *error = ERR_EXTERNAL;
  perror("stat");
  return PT_ERROR;
}


bool isDirectory(PTH path, int *error){
  int er = 0;
  PATH_TYPE type = getPathType(path, &er);

  if (type == PT_ERROR || er != 0) {
    *error = er;
    return false;
  }

  return type == PT_DIRECTORY;
}

bool isFile(PTH path, int *error){
  int er = 0;
  PATH_TYPE type = getPathType(path, &er);

  if (type == PT_ERROR || er != 0) {
    *error = er;
    return false;
  }

  return type == PT_FILE;
}

bool canRead(PTH path){
  return access(path, R_OK);
}

bool canWrite(PTH path){
  return access(path, R_OK);
}

bool canExecute(PTH path){
  return access(path, R_OK);
}

void free_vec_with_content_strings(str_vec vec){
  int num_items = vector_size(vec);
  for(int i=0; i<num_items; i++){
    if(vec[i] != NULL)
      free(vec[i]);
    vec[i] = NULL;
  }
  vector_free(vec);
}

void free_vec_with_content_path_elements(pth_el_vec vec){
  int num_items = vector_size(vec);
  for(int i=0; i<num_items; i++){
    if(vec[i].value != NULL)
      free(vec[i].value);
    vec[i].value = NULL;
  }
  vector_free(vec);
}

void print_str_vec(str_vec vec){
  int num_items = vector_size(vec);
  printf("[");
  for (int i=0; i<num_items; i++) {
    printf("%s, ", vec[i]);
  }
  printf("\b\b]\n");
}

void print_pth_el_vec(pth_el_vec vec){
  int num_items = vector_size(vec);
  printf("[");
  for (int i=0; i<num_items; i++) {
    printf("%s (@%d,%d,%ld), ", vec[i].value, vec[i].line_number, vec[i].token_position, vec[i].file_pos);
  }
  printf("\b\b]\n");
}

pth_el_vec getPathsFromFile(FILE * f){

  rewind(f);

  pth_el_vec path_vec = vector_create();
  size_t len = 0;
  char * line = NULL;
  int nread;

  char line_tok_buffer[10000];

  int line_no = -1;
  while ((nread = getline(&line, &len, f)) != -1) {

    line_no++;
    char* orig_line = line;

    if(nread > 10000){
      fprintf(stderr, "Cannot handle lines longer than 10000 characters, read %d\n", nread);
      fflush(stderr);

      free_vec_with_content_path_elements(path_vec);

      free(orig_line);
      line = NULL;
      orig_line = NULL;

      flock(fileno(f), LOCK_UN);
      fclose(f);

      exit(1);
    }

    // trim leading whitespace
    char c = *line;
    while(isblank(c)){
      line++;
      c = *line;
    }

    // check if line starts with export
    if (strncmp(line, "export ", 7) == 0){
      // move line pointer to after "export "
      line += 7;
    }

    // trim leading whitespace
    c = *line;
    while(isblank(c)){
      line++;
      c = *line;
    }

    if(strncmp(line, "PATH=", 5) != 0){
      // if the rest of the line does not start with "PATH=", continue
      free(orig_line);
      line = NULL;
      orig_line = NULL;
      continue;
    }

    // line starts with "PATH=", skip it
    line += 5;

    // now line shall contain only assigned value

    if(*line == '\0'){
      // If line is empty, continue
      free(orig_line);
      orig_line = NULL;
      line = NULL;
      continue;
    }

    // find $PATH in the assigned value, if it is not present, then the assignment
    // overrides previous value and vector is cleared
    if(strstr(line, "$PATH") == NULL){
      free_vec_with_content_path_elements(path_vec);
      path_vec = vector_create();
    }

#ifdef DEBUG_FPOS
    printf("Remaining line: %s\n", line);
#endif
    int nread_after_trim = nread - (line - orig_line);

    // start strtok_r, split by ':'
    // A copy of line is created, because strtok modifies the string
    strcpy(line_tok_buffer, line);
    char* saveptr = NULL;
    char* tok = strtok_r(line_tok_buffer, ":", &saveptr);
    int token_pos = -1;

    do {
      token_pos++;

      // trim the end (deleting blank characters, ', " and /)
      int i = strlen(tok);
      while(i>=0 && (isspace(tok[--i]) || tok[i] == '\'' || tok[i] == '"' || tok[i] == '/'))
        tok[i] = '\0';

      // trim start (deleting blank characters, ' and ")
      while((isspace(*tok) || *tok == '\'' || *tok == '"' ) && *tok != '\0')
        tok++;

#ifdef TOK_DEBUG
      printf("TOK: %s -> [ ", tok);
      for(int i=0; i<strlen(tok); i++)
        printf("%2X ", tok[i]);
      putchar(']');
      putchar('\n');
#endif // TOK_DEBUG

      // tok has single path element or "$PATH"
      // if it is "$PATH", then omit it
      if(strcmp(tok, "$PATH") == 0)
        continue;


      // tok has single path element

      // copy it to avoid being freed
      char* tok_cpy = malloc(sizeof(char) * strlen(tok) + 1);
      strcpy(tok_cpy, tok);

#ifdef TOK_DEBUG
      printf("TOK_CPY: %s -> [ ", tok_cpy);
      for(int i=0; i<strlen(tok_cpy); i++)
        printf("%2X ", tok_cpy[i]);
      putchar(']');
      putchar('\n');
#endif // TOK_DEBUG

      // insert copied element to vector
      /* vector_add(&path_vec, tok_cpy); */
      PATH_ELEMENT* temp = vector_add_asg(&path_vec);
      temp->line_number = line_no;
      temp->token_position = token_pos;
      temp->value = tok_cpy;
#ifdef DEBUG_FPOS
      printf("TOK: %p\n", tok);
      printf("LINE: %p\n", line_tok_buffer);
      printf("FTELL: %ld, nread_after_trim: %d, (tok-line_tok_buffer): %ld\n", ftell(f), nread_after_trim, tok-line_tok_buffer);
#endif
      // file position is calculated because ftell(f) points to the end of the line
      temp->file_pos = ftell(f) - nread_after_trim + (tok - line_tok_buffer);
      temp = NULL;

    } while((tok = strtok_r(NULL, ":", &saveptr)) != NULL);

    // path_vec should contain PATH assigned values for this and previous lines.

    free(orig_line);
    orig_line = NULL;
    line = NULL;
  }

  // path_vec should contain all PATH elements from entire file
  return path_vec;
}

/*
** Places coursor in file on the begining of nth line (starts at 0)
 */
void fseek_lines(FILE * f, int n){
  fseek(f, 0, SEEK_SET);
  size_t buf_len = 1;
  char* buf = malloc(1);

  while(n-->0)
    getline(&buf, &buf_len, f);

  free(buf);
}

PATH_ELEMENT* addPathToFile(FILE* f, PTH path_to_insert, PATH_ELEMENT * last_known_element_in_file){

  /*
  // Theoretically, now there can be one of:
  // - ':' and next $PATH term
  // - '\'' or '"' anding path assignment value
  // - ' ', '\t' or similar (trailing whitespace)
  // - trailing whitespace and/or comment (#.*)
  // - ';' (and possibly next statement)
  // - ... more?

  // We can debate over the best way to add a path to file, things to consider:
  // - We can add new line at the end of the file
  //   - Pros:
  //     - Easy adding (append to the file)
  //     - Easy removing in the future (delete the line if it is the only path in the line)
  //     WARNING: remember to keep export keyword on the last PATH assignment
  //     - No dealing with searching for the right spot in the line
  //     - No insertion in the middle of the file
  //   - Cons:
  //     - Every added path elements adds another assignment
  //
  // - We can append to the last known line with PATH assignment
  //   - Pros:
  //     - All path elements in one assignment
  //     - No redundant export keywords
  //   - Cons:
  //     - Requires search for the optimal spot
  //     - Requires keeping everything after intact (insertion in the middle of the file)
  //     - Requires careful splicing to remove the path element
  //
  // I believe, that the first option is better. Therefore it is the one being used
  */



  // We insert at the end of the file
  fseek(f, 0, SEEK_END);

  char* buf = malloc(PATH_MAX);

  // Create new element
  PATH_ELEMENT* el = malloc(sizeof(PATH_ELEMENT));
  el->value = buf;

  resolvePath(path_to_insert, el->value);

  // this is the starting position for seeking
  long first_pos = ftell(f);
  el->file_pos = first_pos + strlen(insert_begin);

  // printf("TO WRITE: " insert_begin "%s" insert_end, buf);
  fprintf(f, insert_begin "%s" insert_end, buf);
  fflush(f);

  // to get the line number we seek from the last known position
  fseek(f, last_known_element_in_file->file_pos, SEEK_SET);

  char *line_ptr = NULL;
  size_t line_len = 0;
  int line_no = last_known_element_in_file->line_number;

  while(ftell(f) < first_pos){
    getline(&line_ptr, &line_len, f);
    line_no++;
  }

  el->line_number = line_no;
  // 0 position is taken by $PATH
  el->token_position = 1;

  return el;
}


bool removePathFromFile(FILE** f_ptr, PTH path_to_file, PATH_ELEMENT* path_element_to_remove){

  FILE* f = *f_ptr;

  fseek(f, 0, SEEK_END);
  int file_len = ftell(f);

  rewind(f);
  char tempfilename[PATH_MAX] = {0};
  sprintf(tempfilename, "%s.temp%d", path_to_file, getpid());
  FILE* temp_f = fopen(tempfilename, "w+");
  flock(fileno(temp_f), LOCK_EX);

  fseek(f, path_element_to_remove->file_pos, SEEK_SET);
  char c = fgetc(f);
  while (c != '\n' && ftell(f) > 1)
  {
    fseek(f, -2, SEEK_CUR);
    c = fgetc(f);
  }
  fseek(f, 1, SEEK_CUR);
  // file pointer should point to first character after \n

  long first_char_point = ftell(f);


  c = fgetc(f);

  // seek the next '\n'
  while((c = fgetc(f)) != '\n');
  long newline_end_point = ftell(f);

  int line_len = newline_end_point - first_char_point + 1;

  rewind(f);

  char* buf = malloc(sizeof(char)* path_element_to_remove->file_pos + 1);

  // we copy file until the beggining of the line
  fread(buf, sizeof(char), first_char_point - 1, f);
  fwrite(buf, sizeof(char), first_char_point - 1, temp_f);
  free(buf);

  // expected length if the line contains only prefix and suffix from this program and the path
  // (of course it might happen that the line contains other path that results int the exact same length,
  // but it is highly unlikely)
  int expected_len = strlen(insert_begin insert_end) + strlen(path_element_to_remove->value);

  // printf("Actual: %d Expected: %d\n", line_len, expected_len);

  if(line_len == expected_len){
    // we omit the whole line
    fseek(f, newline_end_point, SEEK_SET);
  }
  else{
    // we omit only our path element

    // if it is not zero then we remove the leading ':', otherwise trailing
    // (if condition true -> extra = -1)
    int extra = -(path_element_to_remove->token_position != 0);

    // amount to be read from the beggining of the line
    int extra_read = path_element_to_remove->file_pos - ftell(f) + extra;
    buf = malloc(extra_read);

    fread(buf, sizeof(char), extra_read, f);
    fwrite(buf, sizeof(char), extra_read, temp_f);
    free(buf);

    // omit element path + 1 character (':')
    fseek(f, strlen(path_element_to_remove->value) + 1, SEEK_CUR);
  }


  // calculate rest length
  int rest_len = file_len - ftell(f);
  buf = malloc(rest_len + 1);

  // copy the rest of the file
  fread(buf, sizeof(char), rest_len, f);
  fwrite(buf, sizeof(char), rest_len, temp_f);


  // unlock and close temp file
  flock(fileno(temp_f), LOCK_UN);
  fclose(temp_f);

  // move temp file in place of the original
  rename(tempfilename, path_to_file);
  unlink(tempfilename);

  free(buf);
  buf = NULL;

  return true;
}

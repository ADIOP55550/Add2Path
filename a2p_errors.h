#ifndef A2P_ERRORS_H_
#define A2P_ERRORS_H_

#include <stdio.h>

#define ERR_EMPTY_PTH 1
#define ERR_EXTERNAL 2

int a2p_errno = 0;

const char* errors[] = {
"", // No error
"Empty path",
"Error in external function (probably in stdlib)"
};

const char* a2p_strerror(int errnum){
    if(errnum < 0 || errnum >= (int) sizeof(errors))
        return "";
    return errors[errnum];
}

void a2p_perror(const char* s){
    int errornum = a2p_errno;

    //char buf[1024];
    const char* colon;
    const char* errstring;

    if(s== NULL || *s == '\0')
        s = colon = "";
    else
        colon = ": ";

    errstring = a2p_strerror(errornum);

    fprintf(stderr, "%s%s%s\n", s, colon, errstring);
}


#endif // A2P_ERRORS_H_

##
# Add2Path
#
# @file
# @version 1.0

SHELL = /bin/sh
# The name of the source files
SOURCES = add2path.c add2pathlib.c c-vector/vec.c
# The name of the executable
EXE = add2path.out
# Flags for compilation
CFLAGS = -Wextra -Wall 
DEBUG_CFLAGS = -ggdb3 -O0 # without optimization
# Flags for linking
LDFLAGS = 
DEBUG_LDFLAGS = -ggdb3
# Libraries to link with
LIBS =
# Use the GCC frontend program when linking
LD = gcc
# This creates a list of object files from the source files
OBJECTS = $(SOURCES:%.c=%.o)

# The first target, this will be the default target if none is specified
# This target tells "make" to make the "all" target
default: all

# Having an "all" target is customary, so one could write "make all"
# It depends on the executable program
all: $(EXE)

debug: CFLAGS += $(DEBUG_CFLAGS)
debug: LDFLAGS += $(DEBUG_LDFLAGS)
debug: all

INFO_PREFIX = "[$$(date +%T)] "
run: $(EXE)
	@echo "$(INFO_PREFIX)Running program in $$(pwd)\n"
	@echo "$(INFO_PREFIX)./$(EXE)"
	@./$(EXE)
	@echo "$(INFO_PREFIX)Process finished with code $$?"


# This will link the executable from the object files
$(EXE): $(OBJECTS)
	$(LD) $(LDFLAGS) $(OBJECTS) -o $(EXE) $(LIBS)

# This is a target that will compiler all needed source files into object files
# We don't need to specify a command or any rules, "make" will handle it automatically
%.o: %.c
	gcc -c $(CFLAGS) $< -o $@

# Target to clean up after us
clean:
	-rm -f $(EXE)      # Remove the executable file
	-rm -f $(OBJECTS)  # Remove the object files


# end

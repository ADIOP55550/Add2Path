#include "add2pathlib.h"
#include "c-vector/vec.h"

#include <argp.h>
#include <errno.h>
#include <limits.h>
#include <linux/limits.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <strings.h>
#include <sys/file.h>
#include <unistd.h>

#define VERSION "1.0"
#define NAME "Add2Path"

const char defaultEnvFilePath[] = "/etc/environment";

const char *argp_program_version = NAME " " VERSION;
const char *argp_program_bug_address = "";

static char doc[] =
    "Adds or removes directories pointed to by PATHS from $PATH environment variable."
    "By default uses /etc/environment as the file to modify. Configurable by --file.";
static char args_doc[] = "[PATHS...]";

static struct argp_option options[] = {
    {"file", 'f', "FILE", 0, "File to search and edit (default /etc/environment)", 0},
    {0}
};

struct arguments {
  str_vec paths;
  char file[PATH_MAX];
  bool restore;
};

static error_t parse_opt(int key, char *arg, struct argp_state *state) {
  struct arguments *arguments = state->input;
  char *val;

  switch (key) {
  case 'f':
    strcpy(arguments->file, arg);
    break;

  case 'r':
    arguments ->restore = true;
    break;

  case ARGP_KEY_ARG:
    val = malloc(sizeof(char) * (strlen(arg) + 1));
    strcpy(val, arg);
    vector_add(&arguments->paths, val);
    break;

  case ARGP_KEY_END:
    break;

  default:
    return ARGP_ERR_UNKNOWN;
  }
  return 0;
}

static struct argp argp = {options, parse_opt, args_doc, doc, 0, 0, 0};

void togglePath(PTH path, FILE* f);
void addPath(PTH path, FILE* f);
void removePath(PTH path, FILE* f);
bool isInPath(PTH path);
PATH_ELEMENT* findElementInVec(PTH path);
void cleanup();

pth_el_vec path_vec = NULL;
struct arguments* g_arguments = NULL;

int main(int argc, const char **argv) {

  struct arguments arguments;
  g_arguments = &arguments;
  strcpy(arguments.file, defaultEnvFilePath);
  arguments.restore = false;
  arguments.paths = vector_create();

  int er = 0;

  // allocate memory and copy strings in argv
  char **new_argv = malloc((argc + 1) * sizeof(char *));

  for (int i = 0; i < argc; ++i) {
    size_t length = strlen(argv[i]) + 1;
    new_argv[i] = malloc(length);
    memcpy(new_argv[i], argv[i], length);
  }
  new_argv[argc] = NULL;

  argp_parse(&argp, argc, new_argv, 0, 0, &arguments);

  // free new_argv
  for (int i = 0; i < argc; ++i)
    free(new_argv[i]);
  free(new_argv);

  int paths_count = vector_size(arguments.paths);
  for (int i = 0; i < paths_count; i++) {
    if(index(arguments.paths[i], '~') != NULL){
      fprintf(stderr, "Please, resolve all ~ characters in the input paths first.\n");
      exit(1);
    }
  }

  char *temp = malloc(sizeof(char) * PATH_MAX + 1);
  // resolve file path - just in case
  resolvePath(arguments.file, temp);
  free(temp);

  strcpy(arguments.file, temp);
  if (!isFile(arguments.file, &er)) {
    fprintf(stderr, "--file should point to a file!");
    exit(1);
  }

  // Open specified file
  FILE *f;
  if((f = fopen(arguments.file, "r+")) == NULL){
    perror("Cannot open file");
    exit(errno);
  }

  flock(fileno(f), LOCK_EX);


  // get all path elements from that file
  path_vec = getPathsFromFile(f);

  // Print them
  printf("Found following $PATH values: \n");
  print_pth_el_vec(path_vec);


  if (vector_size(arguments.paths) == 0) {
    // Command run with no positional arguments
    // get cwd and add/remove it
    char cwd[PATH_MAX];
    getcwd(cwd, sizeof(cwd));
    printf("No paths passed, using cwd: %s\n", cwd);
    togglePath(cwd, f);
  }
  else {
    int paths_count = vector_size(arguments.paths);
    for (int i = 0; i < paths_count; i++) {
      togglePath(arguments.paths[i], f);
    }
  }

  flock(fileno(f), LOCK_UN);
  fclose(f);

  cleanup();
  return 0;
}

bool isInPath(PTH path) {
  printf("Checking path %s... \t", path);
  int count = vector_size(path_vec);
  char pth1[PATH_MAX], pth2[PATH_MAX];
  resolvePath(path, pth1);

  for (int i = 0; i < count; i++) {
    PATH_ELEMENT el = path_vec[i];
    resolvePath(el.value, pth2);
    // printf("Compare %s[%s] to %s[%s]\n", pth1, path, pth2, el.value);

    if (strcmp(pth1, pth2) == 0){
      printf("Path is in vector.\n");
      return true;
    }
  }
  printf("Path is NOT in vector.\n");
  return false;
}

PATH_ELEMENT* findElementInVec(PTH path){
  int count = vector_size(path_vec);
  char pth1[PATH_MAX], pth2[PATH_MAX];
  resolvePath(path, pth1);

  for (int i = 0; i < count; i++) {
    resolvePath(path_vec[i].value, pth2);

    if (strcmp(pth1, pth2) == 0)
      return &path_vec[i];
  }

  return NULL;
}

void addPath(PTH path, FILE* f) {
  printf("Adding path: %s\n", path);
  char pth[PATH_MAX];
  resolvePath(path, pth);

  PATH_ELEMENT *el = addPathToFile(f, pth, &path_vec[vector_size(path_vec) - 1]);

  vector_add(&path_vec, *el);
}

void removePath(PTH path, FILE* f){
  printf("Removing path: %s\n", path);
  PATH_ELEMENT* el = findElementInVec(path);
  if(el == NULL)
    return;

  removePathFromFile(&f, g_arguments->file, el);

}

void togglePath(PTH path, FILE* f){
  char pth[PATH_MAX];
  resolvePath(path, pth);
  if(isInPath(pth))
    removePath(path, f);
  else
    addPath(path, f);
}

void cleanup() {
  free_vec_with_content_path_elements(path_vec);
  path_vec = NULL;
}

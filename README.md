# Add2Path

Program for adding and removing directories from $PATH on Unix systems.

## Compilation

It is advised to compile with make

### Compilation with make

```
make
```

### Manual compilation

```bash
gcc -c -Wextra -Wall add2path.c
gcc -c -Wextra -Wall add2pathlib.c
gcc -c -Wextra -Wall c-vector/vec.c
gcc  add2path.o add2pathlib.o vec.o -o add2path.out 
```

### Debug compilation with make

Use this if you want to add debugging information and disable compiler optimizations

```
make debug
```

### Cleaning output files

You can remove all compilation .o files and .out executable with

```
make clean
```


## Usage

Short version

```
add2path.out [-?V] [-f FILE] [--file=FILE] [--help] [--usage]
            [--version] [PATHS...]
```

Long help: 

```
Usage: add2path.out [OPTION...] [PATHS...]
Adds or removes directories pointed to by PATHS from $PATH environment
variable.By default uses /etc/environment as the file to modify. Configurable
by --file.

  -f, --file=FILE            File to search and edit (default
                             /etc/environment)
  -?, --help                 Give this help list
      --usage                Give a short usage message
  -V, --version              Print program version
```

When program is not provided with PATHs the cwd (current directory) is used.

By default program modifies /etc/environment. To write that file, root privilages are often required.
When using default file consider running this program with sudo privilages.

File to edit can be customized using --file (-f) option. Make sure that specified path exists.


## Creator

© Adrian Klamut 2023

Systemy Operacyjne II
Uniwersytet Rzeszowski


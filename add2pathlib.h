#ifndef ADD2PATHLIB_H_
#define ADD2PATHLIB_H_

#include <stdbool.h>
#include <stdio.h>
#include "c-vector/vec.h"

typedef char *PTH;
typedef char** str_vec;
typedef enum { PT_ERROR, PT_FILE, PT_DIRECTORY, PT_OTHER } PATH_TYPE;

typedef struct {
    char* value;
    int line_number;
    int token_position;
    long file_pos;
} PATH_ELEMENT;

typedef PATH_ELEMENT* pth_el_vec;

// extern int errno;

/*
** Resolve path to an absolute path using realpath function from stdlib.h
** The realpath() function derives, from the path name pointed to by file_name,
** an absolute path name that names the same file, whose resolution does not involve ".","..", or symbolic links.
** The generated path name is stored, up to a maximum of PATH_MAX bytes, in the buffer pointed to by resolved_name.
**
** If successful, realpath() returns a pointer to the resolved name.
** If unsuccessful, the contents of the buffer pointed to by resolved_name are undefined,
** realpath() returns a NULL pointer and sets errno to one of the specified values.
**
** If realpath was unsuccessful, resolvePath returns non-zero code. (possibly ERR_EXTERNAL)
 */
int resolvePath(PTH path, char* buffer);

/*
** Returns true if path is relative (not starting with '/'), false otherwise.
** If path is empty or null, returns false.
** Error is stored in provided integer pointer.
*/
bool isPathRelative(PTH path, int *error) ;

/*
** Returns true if path is absolute (starting with '/'), false otherwise.
** If path is empty or null, returns false.
** Error is stored in provided integer pointer.
 */
bool isPathAbsolute(PTH path, int *error);

/*
** Determines the type of entity pointed to by path
** Returns PT_FILE, PT_DIRECTORY or PT_OTHER.
** If path is empty returns PT_ERROR
** On error, sets error number in the given integer pointer and returns PT_ERROR
**
** uses stat function from sys/stat.h
 */
PATH_TYPE getPathType(PTH path, int *error) ;

/*
** Returns true if path leads to directory, false otherwise
*/
bool isDirectory(PTH path, int *error);

/*
** Returns true if path leads to file, false otherwise
*/
bool isFile(PTH path, int *error);

/*
** Returns true if current process can read path, false otherwise
*/
bool canRead(PTH path);

/*
** Returns true if current process can write to path, false otherwise
*/
bool canWrite(PTH path);

/*
** Returns true if current process can execute path, false otherwise
*/
bool canExecute(PTH path);

/*
** Searches given FILE for assignments to $PATH.
** Also includes export assignments.
** After those are found, array of strings is constructed by spliting
** each assiged value by ':' and appending to the array.
** If substring "$PATH" is not found in one of the assignments, then the array
** is cleared (this assignment would override $PATH instead of appending).
** Then the array is returned.
 */
pth_el_vec getPathsFromFile(FILE* f);

/*
** Searches for the closest spot to insert given path into file f
** If siutable spot is found, the element is created, inserted into file and returned.
** If no spot is found or error happens, NULL is returned.
** Search starts at the position marked by last_known_element_in_file
*/
PATH_ELEMENT* addPathToFile(FILE* f, PTH path_to_insert, PATH_ELEMENT* last_known_element_in_file);

bool removePathFromFile(FILE** f_ptr, PTH path_to_file, PATH_ELEMENT* path_element_to_remove);


void free_vec_with_content_strings(str_vec vec);
void free_vec_with_content_path_elements(pth_el_vec vec);
void print_str_vec(str_vec vec);
void print_pth_el_vec(pth_el_vec vec);



#endif // ADD2PATHLIB_H_
